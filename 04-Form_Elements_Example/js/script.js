// -------------------- Example: Form Events --------------------

/* const sendButton = document.querySelector('.myButton');
sendButton.addEventListener('click', function (event) {
    console.log(event);
    event.preventDefault(); // This will prevent the default submit event behavior for myForm
    console.log('Send Form!');
}) */
// If this code is uncommented, the code for the submit event associated with myForm will have no effect.

// In this example, we will create an object that will store the data received from the form.
let data = {
    userName: '',
    email: '',
    message: ''
};

const userName = document.querySelector('#userName');
const email = document.querySelector('#email');
const message = document.querySelector('#message');
const myForm = document.querySelector('.myForm');

userName.addEventListener('input', readData);
email.addEventListener('input', readData);
message.addEventListener('input', readData);

function readData(e) {
    data[e.target.id] = e.target.value;
    console.log(data);
    // Behavior example: The user writes their name.
    // The target for the input event will be the element with id 'userName'.
    // Data is an object, and its fields can be accessed this way: data[username], data[email], data[message].
    // Because the target's id (userName) is named the same as the field of the object (userName), the
    // object's field will be filled with the target's value, which is the name the user has written in the
    // name input element. The same behavior will apply for the other fields/ids.
}

// When working with forms, good practice is to use the submit event rather than the click event.
// While the click event is associated with the button (although it can be associated with any element, such as text,
// pictures, titles, etc.), the submit event is associated with a form.
myForm.addEventListener('submit', function (event) {
    event.preventDefault();
    
    const { userName, email, message } = data; // Destructuring

    if (userName === '' || email === '' || message === '') {
        /* console.log('You must fill all the fields'); */
        /* displayError('You must fill all the fields'); */
        displayNotification('You must fill all the fields', true);
        return;
    }
    /* console.log('Form submitted!');
    console.log(data.userName);
    console.log(data.email);
    console.log(data.message); */
    /* submittedSuccessfully('Form submitted successfully'); */
    displayNotification('Form submitted successfully');
});

/* function displayError(errorMessage) {
    const error = document.createElement('P');
    error.textContent = errorMessage;
    error.classList add('myError');
    myForm.appendChild(error);
    // Make the error message disappear after a while
    setTimeout(() => {
        error.remove();
    }, 3000);

    // console.log(error);  // No user checks the console for errors
}

function submittedSuccessfully(successMessage) {
    const formSuccess = document.createElement('P');
    formSuccess.textContent = successMessage;
    formSuccess.classList add('success');
    myForm.appendChild(formSuccess);
    // Make the success message disappear after a while
    setTimeout(() => {
        formSuccess.remove();
    }, 3000);
} */

// Refactoring code to make it shorter:
function displayNotification (notificationMsg, error = null) { // Initially, error is null, so the success code 
                                                               // will be executed unless the function call   
    const notification = document.createElement('P');           // has 'true' as the second parameter
    notification.textContent = notificationMsg;
    if (error) { notification.classList.add('myError'); }
    else { notification.classList.add('success'); }
    myForm.appendChild(notification);
    // Make the message disappear after a while
    setTimeout(() => {
        notification.remove();
    }, 3000);
}
