// ------------------------------ querySelector ------------------------------
// We can use querySelector to reference HTML elements, classes, IDs, etc., similar to CSS selectors.

const heading = document.querySelector('.myClass h2'); 
// querySelector returns either 0 (null) or a single element (even if there are multiple matches for the selector).
// If there are no matches for the selector, querySelector will return null without any error or warning.
console.log(heading);
// Note that the selector here uses a class and an element. This is useful when there are multiple h2 elements in the document.

 
// Another option is to use the element's ID (remember, IDs are unique).
// It's recommended to use IDs for JavaScript selectors and not for CSS styling.
const heading2 = document.querySelector('#heading'); 
console.log(heading2);  

// In Firefox Developer Edition, printing this h2 element will provide a lot of information, including its classes (classList), content, etc.

// We can use JavaScript to modify the HTML elements we have selected.
heading2.textContent = 'New Title (modified by a JavaScript script)';
heading.classList.add('myNewClass'); // Inspect the site to verify that this class was added.

// ------------------------------ querySelectorAll ------------------------------
// Unlike querySelector, querySelectorAll can return multiple HTML elements if there are multiple matches for the provided selector.

const myLinks = document.querySelectorAll('.link_container a');
console.log(myLinks); // This will print a "node list" that contains all of those links.

// We can also access a single element among all of the HTML elements found by querySelectorAll
// and modify their properties.
console.log(myLinks[0]); // Print the first element.
myLinks[0].textContent = 'New text';
myLinks[0].href = 'http://google.com';
myLinks[0].classList.add('myNewClass');
myLinks[0].classList.remove('myNewClass');

// ------------------------------ getElementById ------------------------------
// Before querySelector (which can select elements generically via CSS selectors), the way to access HTML elements
// in JavaScript was using getElementById (by ID, class, etc).
const heading3 = document.getElementById('third_link'); // Note that # is not required here.
console.log(heading3);
heading3.textContent = 'Third Link text';
