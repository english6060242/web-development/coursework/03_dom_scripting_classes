// ------------------------------ Generate a new HTML element ------------------------------

// It's recomended that the new elements are created using the name of the element in upper case.
const newLink = document.createElement('A'); 

// We must also fill the link's content
// Add href
newLink.href = 'http://google.com';

// Add text
newLink.textContent = 'A new Link';

// Add class
newLink.classList.add('myClass');

console.log(newLink);

// Up until now, the element only exists in the js code. 
// We must add it to the document in order for it to be visible.
// First we select where we want to add the ellement:
const body = document.querySelector('body');
body.appendChild(newLink); // And then we add it using appendChild
